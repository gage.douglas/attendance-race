import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

time_frame = 3 * 60
driver = webdriver.Chrome()
start_time = time.time()

while time.time() - start_time < time_frame:
    try:
        driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")
        sso_button = driver.find_element(By.CSS_SELECTOR, "control")
        sso_button.click()
        email_field = driver.find_element(By.ID, 'email')
        password_field = driver.find_element(By.ID, 'password')
        email_field.send_keys("I.AM.BATMAN@gotham.pd")
        password_field.send_keys("NANANANANANANA")
        sign_in_button = driver.find_element(By.CSS_SELECTOR, "primary")
        sign_in_button.click()
        #driver.refresh()
        wait = WebDriverWait(driver, 2)
        print("we are waiting for you...")
        token_element = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.tag.is-big")))
        print("its not here, help me look for it!")
        token = token_element.text
        print("MY PRECIOUS")
        token_input = driver.find_element(By.CSS_SELECTOR, "['code']['4']")
        token_input.clear()
        token_input.send_keys(token)
        break
    except Exception as e:
        print("!!SUPER NO BUENO, SENIOR!!:", e)
    time.sleep(.1)

driver.quit()
