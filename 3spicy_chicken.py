import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException

time_frame = 3 * 60
driver = webdriver.Chrome()
start_time = time.time()


while time.time() - start_time < time_frame:
    try:
        driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")
        sso_button = driver.find_element(By.CSS_SELECTOR, "button.")
        sso_button.click()
        time.sleep(1)
        email_field = driver.find_element(By.ID, 'email')
        password_field = driver.find_element(By.ID, 'password')
        email_field.send_keys("g*************m")
        password_field.send_keys("HAHA.no")
        sign_in_button = driver.find_element(By.CSS_SELECTOR, "input")
        sign_in_button.click() #this block is all LOGIN

        # Waiting for the token to become available to click
        xpath_expression = "//span[@div='tag is-bigger]"
        token_element = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, xpath_expression)))

        #using the xpath 'string' is not enough because you need the value of the string
        token = token_element.text
        print("We have the nuclear football")

        token_input = driver.find_element(By.CSS_SELECTOR, "['code']['4']")
        token_input.clear() #clears field, its like when you have to double click to enter the field
        token_input.send_keys(token) #enters token into field and submits
        print('The Eagle has landed', token)

        break
    except TimeoutException: #Rev limiter
        print("it is what it is.")
        driver.refresh()
    except StaleElementReferenceException: #If something moves, I find it
        print("Element no esta aki Refresh-ese.")
        driver.refresh()
    except Exception as e: #Big error due to element availability
        print("SUPER NO BUENO, SENIOR:", e)
    time.sleep(1) #polling rate of Rev Limiter

driver.quit()
