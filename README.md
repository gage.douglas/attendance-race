So you want to get the golden snitch huh?
or maybe you just want to not worry about grabbing the token for attendence.

what ever your reason is, this script will help. 

I don't believe in handouts, only hand-ups! 

So the code intentionally includes some challenges (landmines) to help you understand its construction and outline the process of building your own.

The main goal of the script is to fetch a dynamically dropped object, referred to as a 'string' here. The script extracts this 'string' from the information on the page, pastes it into an available field, and then submits it.

While the script may seem daunting at first, understanding a few key aspects will help everything fall into place. Anything to the left of an '=' is correct and follows explicit naming conventions. Anything to the right of an '=' is open to changes, so that's where you should focus your attention. Check the left side to understand what you're looking for, and the right side indicates what you're doing. For example, if you're searching for a CSS selector, ensure it's formatted correctly on the right side. The naming of the 'string' value may also need to be more explicit.

You can find all the information used to write this script at https://www.selenium.dev/. Best of luck in catching your golden snitch!
