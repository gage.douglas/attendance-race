import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


time_frame = 3 * 60
driver = webdriver.Chrome()
start_time = time.time()

while time.time() - start_time < time_frame:
    try:
        driver.get("https://sis.galvanize.com/cohorts/69/attendance/mine/")
        sso_button = driver.find_element_by_css_selector("control")
        sso_button.click()
        #time.sleep(2)
        email_field = driver.find_element_by_id('email')
        password_field = driver.find_element_by_id('password')
        email_field.send_keys("g****************m")
        password_field.send_keys("nope")
        sign_in_button = driver.find_element_by_css_selector("input")
        sign_in_button.click()
        token_element = driver.find_element_by_css_selector("['code']['4']")
        text_field = driver.find_element_by_id('form-token')
        token = token_element.get_attribute('value')
        text_field.send_keys(token)

        break
    except Exception as e:
        print("ITS NOT ALRIGHT!", e)
    time.sleep(1)
driver.quit()


#this opens into a new browser! Import Cookies? too hard! have to make it log in for me
